Category.create!([
  {name: "c1"},
  {name: "c2"}
])
Item.create!([
  {name: "1-1", price: nil, category_id: 1},
  {name: "1-2", price: nil, category_id: 1},
  {name: "1-3", price: nil, category_id: 1},
  {name: "2-1", price: nil, category_id: 2},
  {name: "2-2", price: nil, category_id: 2}
])
