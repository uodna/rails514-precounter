### activerecord-precounter

```
categories = Category.all
ActiveRecord::Precounter.new(categories).precount(:items)

Category Load (0.1ms)  SELECT  "categories".* FROM "categories" LIMIT ?  [["LIMIT", 11]]
(0.1ms)  SELECT COUNT(*) AS count_all, "items"."category_id" AS items_category_id FROM "items" WHERE "items"."category_id" IN (SELECT "categories"."id" FROM "categories") GROUP BY "items"."category_id"

categories.each do |c|
  p c.items_count
end

3
2
```

### activerecord-precount

```
Category.all.precount(:items).each do |c|
  p c.items.count
end

Category Load (0.2ms)  SELECT "categories".* FROM "categories"
 (0.3ms)  SELECT COUNT("items"."category_id") AS count_category_id, "items"."category_id" AS items_category_id FROM "items" WHERE "items"."category_id" IN (1, 2) GROUP BY "items"."category_id"
3
2
```

